import jetbrains.buildServer.configs.kotlin.*
import jetbrains.buildServer.configs.kotlin.buildFeatures.dockerSupport
import jetbrains.buildServer.configs.kotlin.buildSteps.DockerCommandStep
import jetbrains.buildServer.configs.kotlin.buildSteps.dockerCommand
import jetbrains.buildServer.configs.kotlin.projectFeatures.dockerRegistry
import jetbrains.buildServer.configs.kotlin.triggers.vcs
import jetbrains.buildServer.configs.kotlin.vcs.GitVcsRoot

version = "2022.10"

project {

    vcsRoot(HttpsGitlabComNgocnhTeamcityAgentPhp74gitRefsHeadsMaster)

    buildType(Build)

    features {
        dockerRegistry {
            id = "PROJECT_EXT_4"
            name = "Docker Registry"
            userName = "ngocnh"
            password = "credentialsJSON:8dcf4ac6-69dc-4725-a254-b7e2b01f4e08"
        }
    }
}

object Build : BuildType({
    name = "Build"

    params {
        password("env.DOCKER_USERNAME", "credentialsJSON:483ad16f-55b6-4e6c-8934-f371eaf19401", label = "DOCKER_USERNAME", display = ParameterDisplay.HIDDEN, readOnly = true)
        param("env.DOCKER_REPO", "ngocnh/teamcity:php-7.4")
        password("env.DOCKER_PASSWORD", "credentialsJSON:8dcf4ac6-69dc-4725-a254-b7e2b01f4e08", label = "DOCKER_USERNAME", display = ParameterDisplay.HIDDEN, readOnly = true)
    }

    vcs {
        root(HttpsGitlabComNgocnhTeamcityAgentPhp74gitRefsHeadsMaster)
    }

    steps {
        dockerCommand {
            name = "Build Image"
            commandType = build {
                source = file {
                    path = "Dockerfile"
                }
                platform = DockerCommandStep.ImagePlatform.Linux
                namesAndTags = "%env.DOCKER_REPO%"
            }
        }
        dockerCommand {
            name = "Publish Image"
            commandType = push {
                namesAndTags = "%env.DOCKER_REPO%"
            }
        }
    }

    triggers {
        vcs {
        }
    }

    features {
        dockerSupport {
            loginToRegistry = on {
                dockerRegistryId = "PROJECT_EXT_4"
            }
        }
    }
})

object HttpsGitlabComNgocnhTeamcityAgentPhp74gitRefsHeadsMaster : GitVcsRoot({
    name = "https://gitlab.com/ngocnh/teamcity-agent-php74.git#refs/heads/master"
    url = "https://gitlab.com/ngocnh/teamcity-agent-php74.git"
    branch = "refs/heads/master"
    branchSpec = "refs/heads/*"
    authMethod = password {
        userName = "ngocnh"
        password = "%env.GIT_ACCESS_TOKEN%"
    }
    param("oauthProviderId", "PROJECT_EXT_2")
})
